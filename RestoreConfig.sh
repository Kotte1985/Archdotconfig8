#!/usr/bin/env bash
#-------------------------------------------------------------------------
#      _          _    __  __      _   _    
#     /_\  _ _ __| |_ |  \/  |__ _| |_(_)__ 
#    / _ \| '_/ _| ' \| |\/| / _` |  _| / _|
#   /_/ \_\_| \__|_||_|_|  |_\__,_|\__|_\__| 
#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------

echo
echo "FINAL SETUP AND CONFIGURATION"


confTerminal1 () {
    echo
    echo "=========="
    echo "Restore From Backup"
    echo "=========="

    ## config for kde plasma
    echo "configs"
    # Kde CONFIGURATION
    tar -xvf configs-backup.tgz -C ~/
    tar -xvf dotfiles-backup.tgz -C ~/
    tar -xvf local-backup.tgz -C ~/
    tar -xvf conky1-backup.tgz -C ~/
    tar -xvf conky2-backup.tgz -C ~/
}

confTerminal1



echo "Done!"
echo
echo "Reboot now..."
echo


