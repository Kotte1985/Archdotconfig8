#!/usr/bin/env bash
#-------------------------------------------------------------------------
#      _          _    __  __      _   _    
#     /_\  _ _ __| |_ |  \/  |__ _| |_(_)__ 
#    / _ \| '_/ _| ' \| |\/| / _` |  _| / _|
#   /_/ \_\_| \__|_||_|_|  |_\__,_|\__|_\__| 
#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------

echo
echo "FINAL SETUP AND CONFIGURATION"


confTerminal1 () {
    echo
    echo "=========="
    echo "CONFIGURATION-1"
    echo "=========="

    ## p10k-zsh
    echo ".p10k.zsh"
    sudo cp -r ~/.dotfiles/User/.p10k.zsh ~/.p10k.zsh

    ## Zshrc
    echo "ZSHRC" 
    rm ~/.zshrc
    ln -s ~/zsh/.zshrc ~/.zshrc

    ## konsole Profile setting
    echo "konsole Profile setting"
    rm ~/.local/share/konsole/Neptune.profile
    sudo cp -r ~/.dotfiles/Neptune.profile ~/.local/share/konsole/Neptune.profile

    # ZSH
    echo "ZSH"
    chsh -s $(which zsh)

    
}

confTerminal1


echo "Done!"
echo
echo "Reboot now..."
echo


