#!/usr/bin/env bash

echo
echo "INSTALLING AUR SOFTWARE"
echo

cd "${HOME}"

echo "CLOING: LIGHTLY BOESH THEME"
sudo pacman -S cmake extra-cmake-modules kdecoration qt5-declarative qt5-x11extras --noconfirm --needed
git clone --single-branch --depth=1 https://github.com/boehs/Lightly.git
cd Lightly && mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=lib -DBUILD_TESTING=OFF ..
make
sudo make install




echo
echo "Done!"
echo
