#!/usr/bin/sudo bash
#-------------------------------------------------------------------------
#      _          _    __  __      _   _
#     /_\  _ _ __| |_ |  \/  |__ _| |_(_)__
#    / _ \| '_/ _| ' \| |\/| / _` |  _| / _|
#   /_/ \_\_| \__|_||_|_|  |_\__,_|\__|_\__|
#  Arch Linux Post Install Setup and Config
#-------------------------------------------------------------------------

echo
echo "FINAL SETUP AND CONFIGURATION"


confTerminal () {
    echo
    echo "=========="
    echo "CONFIGURATION-2"
    echo "=========="

    ## wallpapers
    echo "wallpapers"
    sudo cp -r ~/.dotfiles/wall /usr/share/wallpapers/Next/contents

    ## Profile Picture
    echo "Profile Picture"
    sudo cp -r ~/.dotfiles/kotte /var/lib/AccountsService/icons/kotte

    ## config for fstab
    echo "fstab"
    sudo cp -r ~/.dotfiles/fstab /etc

    ## config for vconsole
    echo "vconsole"
    sudo cp -r ~/.dotfiles/vconsole.conf /etc

    ## config for mkinitcpio.conf
    echo "mkinitcpio.conf"
    sudo cp -r ~/.dotfiles/mkinitcpio.conf /etc     

    ## Zshrc
    echo "ZSHRC"
    sudo cp -r ~/.dotfiles/Root/.zshrc /root/.zshrc

    ## Powerlevel10
    echo "Powerlevel10"
    sudo cp -r /home/kotte/.dotfiles/powerlevel10k /root
    sudo cp -r /home/kotte/.dotfiles/Root/.p10k.zsh /root/.p10k.zsh

    # ZSH
    echo "ZSH"
    sudo chsh -s $(which zsh)


}


confTerminal


echo "Done!"
echo
echo "Reboot now..."
echo


